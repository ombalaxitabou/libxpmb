#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#include "xpmb.h"

/*
 * Set value for alfa channel of color 'none' (transparent)
 * and other colors (opaque)
 */
#define COLOR_TRANSPARENT 0
#define COLOR_OPAQUE 255

/*
 * First time memory allocation for symbolic names is calculated by this.
 * The higher the value the more chance the allocated memory would be
 * sufficient and no need to extende. Of course it would take more space.
 */
#define AVERAGE_SYM_NAME_LENGTH 10

/* Please define this at compile time */
#ifndef SHARED_DIR
#define SHARED_DIR "/usr/share/X11"
#endif

/*
 * Filename of the file with X11 color names and values.
 * SHARED_DIR is searched for this file.
 */
#define COLOR_NAMES_FILE "rgb.txt"

/*
 * Buffer uses this amount of bytes.
 */
#define BUFFER_MAX 1024

struct _xpm_image_t {
	int		  columns;	/* width of image		*/
	int		  rows;		/* height of image		*/
	int		  colors;	/* number of available colors	*/
	int		  cpp;		/* chars per pixel		*/
	int		  bg_no; 	/* number of color 'None'	*/
	unsigned int	  bg[4]; 	/* color for 'None'		*/
	char		 *color_char;	/* color code memory location	*/
	unsigned int	 *color_rgba;	/* color values memory location	*/
	char		 *sym_names;	/* sym names memory location	*/
	int		  sym_names_len;/* sym names memory length	*/
#if COUNT_COLORS_OPTION
	int		  count_colors; /* yes / no ?			*/
	unsigned int	 *color_count;	/* color counter		*/
#endif
	char		**data;		/* image data			*/
	};

/*
 * Simply read a file into memory
 */
int
read_file_to_buffer (char *filename, char **ret) {
	FILE		*fd;

	char		 buffer[BUFFER_MAX];
	char		*p;
	int		 i, l, b;

	fd = fopen (filename, "r");
	if (!fd)
		return B_INPUT;

	for (i = 0, l = 0; fgets (buffer, BUFFER_MAX, fd); i++)	{
		/* strlen () does not count terminating null byte */
		b = strlen (buffer);
		p = realloc (*ret, l + b + 1);
		if (!p) {
			free (*ret);
			*ret = NULL;
			fclose (fd);
			return B_INSUF;
			};

		*ret = p;
		memcpy (*ret + l, buffer, b + 1);
		l += b;
		};

	fclose (fd);
	return B_SUCCESS;
	};

/*
 * Remove C syntax from buffer, like if the file was included into source code
 */
int
parse_buffer_to_xpm_data (char *buffer, char ***data) {
	char *b;
	int i, j, ret;
	unsigned int columns, rows, colors, cpp;

	for (b = buffer; *b != '"' && *b != '\0'; b++);
	ret = sscanf (b + 1, "%d %d %d %d", &columns, &rows, &colors, &cpp);
	if (ret < 4)
		return B_INVAL;

	*data = malloc ((1 + colors + rows) * sizeof (char **));
	if (!*data)
		return B_INSUF;

	memset (*data, 0, ((1 + colors + rows) * sizeof (char **)));

	for (j = 0; *b != '\0' && j < 1 + colors + rows; j++) {
		/* find the end of line */
		for (i = 0; *(b + i + 1) != '"' && *(b + i + 1) != '\0'; i++);
		if (*(b + i + 1) != '"')
			return B_INVAL;

		(*data)[j] = strndup (b + 1, i);

		/* find the next line (or end of file) */
		for (b = b + i + 2; *b != '"' && *b != '\0'; b++);
		};

	return B_SUCCESS;
	};


static int
colors_allocate_memory (xpm_image_t *xpm) {
	size_t length;

	if (!xpm)
		return B_INPUT;

	length = xpm->colors * xpm->cpp * sizeof (unsigned char);
	xpm->color_char = malloc (length);
	if (!xpm->color_char)
		return B_INSUF;

	length = xpm->colors * 4 * sizeof (unsigned int);
	xpm->color_rgba = malloc (length);
	if (!xpm->color_rgba) {
		free (xpm->color_char);
		return B_INSUF;
		};

#if COUNT_COLORS_OPTION
	if (xpm->count_colors) {
		length = xpm->colors * sizeof (unsigned int);
		xpm->color_count = memset (malloc (length), 0, (length));
		if (!xpm->color_count) {
			free (xpm->color_char);
			free (xpm->color_rgba);
			return B_INSUF;
			}
		};
#endif

	return B_SUCCESS;
	};


static int
symbolic_names_allocate_memory (xpm_image_t *xpm) {
	size_t length;

	if (!xpm || xpm->sym_names)
		return B_INPUT;

	length = AVERAGE_SYM_NAME_LENGTH * xpm->colors * sizeof (char);
	xpm->sym_names = memset (malloc (length), 0, (length));
	if (!xpm->sym_names)
		return B_INSUF;

	xpm->sym_names_len = length;

	return B_SUCCESS;
	};

/*
 * Find the next word in string.
 * Next word is defined as the first non white space character
 * after the next white space sequence.
 */
static inline char *
next (char *p) {
	while ((*p != ' ') && (*p != '\t') && (*p != '\n') && (*p != '\0')) p++;
	while ((*p == ' '  ||  *p == '\t') && (*p != '\n') && (*p != '\0')) p++;

	if (*p == '\n' || *p == '\0') return NULL;

	return p;
	};

/*
 * Read X11 color names file 'rgb.txt', find a named color and
 * fill its value into memory location set by caller of function.
 */
static int
get_color_by_x11_name (
		char		*cvalstr,	/* color value string		*/
		int		 length,	/* length of color string	*/
		unsigned int	*rgba) {	/* copy identified color vaules *
						 * to this memory location	*/

	char  buffer[BUFFER_MAX];
	FILE *x_color_names = NULL;
	int   i, ret;

	if (!cvalstr || length <= 0 || !rgba)
		return B_INPUT;

	ret = snprintf (buffer, BUFFER_MAX, "%s/%s", SHARED_DIR, COLOR_NAMES_FILE);
	if (ret >= BUFFER_MAX)
		return B_INSUF;

	x_color_names = fopen (buffer, "r");
	if (!x_color_names)
		return B_FMISS;

	while (fgets (buffer, BUFFER_MAX, x_color_names)) {
		char *p = buffer;

		/* comment in rgb.txt */
		if (*p == '!')
			continue;

		/* find first character of color name string */
		while (((*p >= '0') && (*p <= '9')) || (*p == ' ') || (*p == '\t'))
			p++;

		/* check length of color name string */
		if (*(p + length) != '\n')
			continue;

		/* compare strings */
		for (i = 0; i < length; i++)
			if (*(p + i) != *(cvalstr + i))
				break;

		if (i != length)
			continue;

		/* found a matching color name */
		ret = sscanf (buffer, "%3d %3d %3d", rgba, rgba + 1, rgba + 2);
		if (ret < 3)
			break;

		/* some error checking */
		if (*rgba       > 255) *rgba       %= 256;
		if (*(rgba + 1) > 255) *(rgba + 1) %= 256;
		if (*(rgba + 2) > 255) *(rgba + 2) %= 256;

		*(rgba + 3) = COLOR_OPAQUE;

		fclose (x_color_names);
		return B_SUCCESS;
		};

	/* set black as default */
	*rgba       = 0;
	*(rgba + 1) = 0;
	*(rgba + 2) = 0;
	*(rgba + 3) = COLOR_OPAQUE;

	fclose (x_color_names);
	return B_INVAL;
	};

/*
 * Parse hexadecimal #RRGGBB color value or
 * get values from 'rgb.txt' for named colors.
 */
static int
parse_color_value (char *p, unsigned int *rgba) {
	int i, ret = 0;

	if (!p || !rgba)
		return B_INPUT;

	/* find out length of color value string */
	for (i = 0;
	    *(p + i) != ' ' &&
	    *(p + i) != '\t' &&
	    *(p + i) != '\0';
	    i++);

	if (*p == '#') {
		/* convert hexa color */
		ret = sscanf (p, "#%2x%2x%2x", rgba, rgba + 1, rgba + 2);
		*(rgba + 3) = COLOR_OPAQUE;

		if (ret < 3)
			return B_INVAL;

		return B_SUCCESS;
		};

	/* 'None' for transparent */
	if ((!strncmp (p, "None", 4))
	|| (!strncmp (p, "none", 4))) {
		/* set transparent black as default */
		*rgba       = 0;
		*(rgba + 1) = 0;
		*(rgba + 2) = 0;
		*(rgba + 3) = COLOR_TRANSPARENT;

		return B_SUCCESS;
		};

	/* lookup color from rgb.txt */
	return get_color_by_x11_name (p, i, rgba);
	};

/*
 * Save symbolic names for later use
 * (for example changing colors of them).
 * White space characters are not allowed.
 */
static int
parse_symbolic_name (char *p, xpm_image_t *xpm) {
	int   j       = 0;
	char *new_mem = NULL;
	int   str_len = 0;
	int   word    = 0;
	char *tmp     = xpm->sym_names;
	char *end     = xpm->sym_names + str_len;

	if (!p || !xpm)
		return B_INPUT;

	/* allocate memory if not already allocated */
	if (!xpm->sym_names) {
		if (symbolic_names_allocate_memory (xpm) < 0)
			return B_INSUF;

		tmp = xpm->sym_names;
		end = xpm->sym_names;
		};

	/* count already saved words */
	while (*tmp != '\0') {
		word++;
		/* space is field separator between words */
		while (*tmp != ' ' && *tmp != '\0') tmp++;
		/* find next word */
		while (*tmp == ' ' && *tmp != '\0') tmp++;
		};

	if (word == xpm->colors)
		/* already parsed all colors, so what is this now? */
		return B_SUCCESS;

	/* find the end of string, no white spaces allowed */
	for (j = 0;
	    p        != NULL && /* just to make sure */
	    *(p + j) != ' '  &&
	    *(p + j) != '\t' &&
	    *(p + j) != '\0';
	    j++);

	str_len = strlen (xpm->sym_names);
	if (str_len + j + 1 > xpm->sym_names_len) {
		/* we need to expand our memory allocation */
		new_mem = realloc (
				xpm->sym_names,
				str_len + (j + 1) * (xpm->colors - word) + 1);
		if (!new_mem)
			/* not enough memory */
			return B_INSUF;

		xpm->sym_names = new_mem;
		tmp = new_mem;
		end = new_mem + str_len;
		/* now we have enough memory allocated for sure */
		};

	if (word) {
		/* this is not the first word, so add a separator */
		*end = ' ';
		end++;
		str_len++;
		};

	strncpy (end, p, j);
	end += j;
	*end = '\0';
	str_len += j;
	word++;

	if (word == xpm->colors) {
		/* free unused memory, no more colors to parse */
		/* we need to expand our memory allocation */
		new_mem = realloc (xpm->sym_names, str_len + 1);
		if (!new_mem)
			/* strange error, we are shrinking memory */
			return B_INSUF;

		xpm->sym_names = new_mem;
		end = new_mem + str_len;
		};

	return B_SUCCESS;
	};

/*
 * Parse color code, color value and symbolic name.
 */
static int
parse_color (char *p, unsigned int current, xpm_image_t *xpm) {
	int		 ret;
	unsigned int	 i;
	int		 c = 0;
	int		 s = 0;

	if (!p || !xpm)
		return B_INPUT;

	unsigned int	 cpp  = xpm->cpp;
	char		*code = xpm->color_char + cpp * current;
	unsigned int	*rgba = xpm->color_rgba +   4 * current;

	/* color code */
	for (i = 0; i < cpp; i++)
		*(code + i) = *p++;

	p = next (p);

	while (p) {
		switch (*p) {
		/* color value */
		case 'c':
			if (c)
				return B_INVAL;

			c++;
			p = next (p);

			/* save position of transparent background color */
			if ((!strncmp (p, "None", 4))
			||  (!strncmp (p, "none", 4)))
				xpm->bg_no = current;

			ret = parse_color_value (p, rgba);
			if (ret)
				return ret;

			break;

		/* symbolic name */
		case 's':
			if (s)
				return B_INVAL;

			s++;
			p = next (p);

			ret = parse_symbolic_name (p, xpm);
			if (ret)
				return ret;

			break;

		/* something else we don't handle */
		default:
			return B_NOSUP;
			};

		p = next (p);
		};

	if (!c)
		return B_INVAL;

	if (!s)
		return parse_symbolic_name (" ", xpm);

	return B_SUCCESS;
	};

/*
 * Parse headers and colors. Data can be an xpm file included in C source
 */
int
parse_xpm_data_to_xpm_image (char **data, xpm_image_t **image) {
	int ret;
	unsigned int columns, rows, colors, cpp;
	unsigned int i;

	if (!data || !image)
		return B_INPUT;

	if (!*image) {
		*image = malloc (sizeof (xpm_image_t));
		if (!*image)
			return B_INSUF;

		};

	memset (*image, 0, sizeof (xpm_image_t));

	/* header */
	ret = sscanf (*data, "%d %d %d %d", &columns, &rows, &colors, &cpp);
	if (ret < 4)
		return B_INVAL;

	if (columns % cpp)
		return B_INVAL;

	(*image)->columns = columns;
	(*image)->rows    = rows;
	(*image)->colors  = colors;
	(*image)->cpp     = cpp;
	(*image)->data    = data;
	/*
	 * Set an invalid value for background color number,
	 * so later we can know if there is no transparent background.
	 */
	(*image)->bg_no   = -1;

	/* colors */
	ret = colors_allocate_memory (*image);

	for (i = 0; i < colors && ret == B_SUCCESS; i++)
		ret = parse_color (
		    data[i + 1],
		    i,
		    *image);

	return ret;
	};

static int
lookup_color (char *p, xpm_image_t *xpm) {
	int	 i, j;
	char	*t;		/* pointer to color code position */

	if (!p || !xpm)
		return B_INPUT;

	if (strnlen (p, xpm->cpp) < xpm->cpp)
		/* misuse of this function */
		return B_INVAL;

	t = xpm->color_char;
	i = 0;
	j = 0;
	while ((j < xpm->cpp) && (i < xpm->colors)) {
		/* matching char in color code, check next char, if any */
		if (*(t + j) == *(p + j)) {
			j++;
			continue;
			};

		/* no match, go to next color */
		i++;                   /* next color number           */
		t += xpm->cpp;         /* pointer to next color code  */
		j = 0;                 /* char position in color code */
		};

	/* found correct one */
	if (j == xpm->cpp && j > 0)
		return i;

	return B_INVAL;
	};


static int
fill_image_row (char *p, xpm_image_t *xpm, unsigned char *q) {
	int i, j, max;

	if (!p || !xpm || !q)
		return B_INPUT;

	if (xpm->columns % xpm->cpp)
		return B_INVAL;

	max = xpm->columns * xpm->cpp;

	/* walk through the line */
	for (j = 0; j < max; j += xpm->cpp, p += xpm->cpp) {
		i = lookup_color (p, xpm);

		if ((i < 0) || (i > xpm->colors)) {
			/* could not find color */
			fprintf (stderr, "Error ocurred when filling image data\n");
			fprintf (stderr, "Could not find color: \"");
			for (i = 0; i < xpm->cpp; i++) fprintf (stderr, "%c", p[i]);
			fprintf (stderr, "\"\n");
			return B_INVAL;
			};

#if COUNT_COLORS_OPTION
		if (xpm->count_colors)
			xpm->color_count[i]++;
#endif

		*q++ = *(xpm->color_rgba + 4 * i + 2);
		*q++ = *(xpm->color_rgba + 4 * i + 1);
		*q++ = *(xpm->color_rgba + 4 * i + 0);
		*q++ = *(xpm->color_rgba + 4 * i + 3);
		};

	return B_SUCCESS;
	};


/*
 * Prepare pixel data suitable for ximage
 */
int
parse_xpm_image_pixel_data (xpm_image_t *image, unsigned char **pixels) {
	size_t length = 0;
	int    ret    = B_SUCCESS;
	int    i;

	if (!image || !pixels)
		return B_INPUT;

	length = 4 * image->columns * image->rows * sizeof (unsigned char);
	*pixels = memset (malloc (length), 0, (length));
	if (!*pixels)
		return B_INSUF;

	for (i = 0; i < image->rows && ret == B_SUCCESS; i++)
		ret = fill_image_row (
		    image->data[i + image->colors + 1],
		    image,
		    *pixels + i * 4 * image->columns);

	return ret;
	};

/*
 * Copy "#RRGGBBAA" format color to nth color value.
 */
static int
xpm_image_set_nth_color_value (xpm_image_t *image, int n, unsigned int *val) {
	unsigned int *p;
	int i;

	if (!image || n < 0 || n > image->colors || !val)
		return B_INPUT;

	p = image->color_rgba + 4 * n;
	for (i = 0; i < 4; i++)
		p[i] = val[i] % 256;

	return B_SUCCESS;
	};

#if COUNT_COLORS_OPTION
/*
 * Enable / disable color counting
 */
int
xpm_image_count_colors (xpm_image_t *image, int set) {
	image->count_colors = set;
	return B_SUCCESS;
	};
#endif

int
xpm_image_get_colums (xpm_image_t *image) {
	return image->columns;
	};

int
xpm_image_get_rows (xpm_image_t *image) {
	return image->rows;
	};

int
xpm_image_get_number_of_colors (xpm_image_t *image) {
	return image->colors;
	};

int
xpm_image_get_chars_per_pixel (xpm_image_t *image) {
	return image->cpp;
	};

/*
 * Set color value for 'None' in "#RRGGBBAA" format
 * Example : "#FF00FFFF"
 */
int
xpm_image_set_transparent_color (xpm_image_t *image, char *color) {
	int ret;
	unsigned int *bg;

	if (!image || !color || image->bg_no < 0 || image->bg_no > image->colors)
		return B_INPUT;

	bg = image->bg;

	ret = sscanf (color, "#%2x%2x%2x%2x", &bg[0], &bg[1], &bg[2], &bg[3]);
	if (ret == 3)
		bg[3] = COLOR_OPAQUE;

	else if (ret != 4)
		return B_INPUT;

	xpm_image_set_nth_color_value (image, image->bg_no, bg);

	return B_SUCCESS;
	};

/*
 * Copy symbolic name of the nth color to buffer upto length.
 * Returns number of copied bytes or negative error code.
 */
int
xpm_image_get_nth_color_name (
		xpm_image_t *image, int n, char *buffer, int length) {

	char *p, *b;
	unsigned int cnt = 0;
	int l = 0;

	if (!image || n < 0 || n > image->colors || !buffer || length <= 1)
		return B_INPUT;

	p = image->sym_names;
	b = buffer;
	while (*p != '\0' && cnt <= n) {
		if (*p == ' ') {
			cnt++;
			p++;
			continue;
			};

		/* copy to buffer */
		if (cnt == n) {
			if (l >= length)
				return B_INSUF;

			*b = *p;
			b++;
			l++;
			};

		p++;
		};

	*b = '\0';

	return l;
	};

/*
 * Copy nth color value in "#RRGGBBAA" format to buffer upto length.
 * Returns number of copied bytes or negative error code.
 */
int
xpm_image_get_nth_color_value (
		xpm_image_t *image, int n, char *buffer, int length) {

	unsigned int *p;
	char *b;
	int l = 0;

	if (!image || n < 0 || n > image->colors || !buffer || length <= 10)
		return B_INPUT;

	p = image->color_rgba + 4 * n;
	b = buffer;
	l = snprintf (b, length, "#%02x%02x%02x%02x", p[0], p[1], p[2], p[3]);
	if (l >= length)
		return B_INSUF;

	return l;
	};

/*
 * Change a color with symbolic name to value ...
 */
int
xpm_image_change_color_by_name (
		xpm_image_t *image, char *color_name, char *color_value) {

	char         buffer[BUFFER_MAX];
	unsigned int val[4];
	int          i, r;

	if (!image || !color_name || !color_value)
		return B_INPUT;

	for (i = 0; i < image->colors; i++) {
		r = xpm_image_get_nth_color_name (image, i, buffer, BUFFER_MAX);
		if (r < 0)
			return r;

		if (strcmp (color_name, buffer))
			continue;

		r = sscanf (color_value, "#%2x%2x%2x%2x",
				&val[0], &val[1], &val[2], &val[3]);

		if (r == 3)
			val[3] = COLOR_OPAQUE;

		else if (r != 4)
			return B_INPUT;

		return xpm_image_set_nth_color_value (
				image, i, val);

		};

	return B_FMISS;
	};

/*
 * Compare two hexadecimal values supplied as ascii strings if they differ;
 * Return zero if equal (no difference).
 */
static int
hex_not_eq (char *a, char *b) {
	/* just to make sure we handle every case */
	if (!a || !b)
		return B_INPUT;

	/*
	 * 48... 57 numbers
	 * 65... 70 upper case letters
	 * 97...102 lower case letters
	 */
	while (
	    /* a is a number and b equals a */
	    ((*a > 47 && *a <  58) &&  *b == *a) ||
	    /* a is an upper case letter and b is the same or same in lower */
	    ((*a > 64 && *a <  71) && (*b == *a || *b == (*a) + 32)) ||
	    /* a is a lower case letter and b is the same or same in upper */
	    ((*a > 96 && *a < 103) && (*b == *a || *b == (*a) - 32))
	) {
		a++;
		b++;
		};

	if (*a == '\0' && *b == *a)
		return B_SUCCESS;

	/* just return something that is not an error nor has other meaning */
	return 1;
	};

/*
 * Change a color of old_value to new_value ...
 */
int
xpm_image_change_color_by_value (
		xpm_image_t *image, char *old_value, char *new_value) {

	char          buffer[BUFFER_MAX];
	unsigned int  val[4];
	int           i, r;

	if (!image || !old_value || !new_value)
		return B_INPUT;

	for (i = 0; i < image->colors; i++) {
		r = xpm_image_get_nth_color_value (image, i, buffer, BUFFER_MAX);
		if (r < 0)
			return r;

		if (!hex_not_eq (old_value, buffer))
			continue;

		r = sscanf (new_value, "#%2x%2x%2x%2x",
				&val[0], &val[1], &val[2], &val[3]);

		if (r == 3)
			val[3] = COLOR_OPAQUE;

		else if (r != 4)
			return B_INPUT;

		return xpm_image_set_nth_color_value (
				image, i, val);

		};

	return B_FMISS;
	};

int
free_xpm_image (xpm_image_t *image) {
	if (!image)
		return B_INPUT;

	if (image->color_char) {
		free (image->color_char);
		image->color_char = NULL;
		};

	if (image->color_rgba) {
		free (image->color_rgba);
		image->color_rgba = NULL;
		};

	if (image->sym_names) {
		free (image->sym_names);
		image->sym_names = NULL;
		};

#if COUNT_COLORS_OPTION
	if (image->color_count) {
		free (image->color_count);
		image->color_count = NULL;
		};
#endif

	free (image);

	return B_SUCCESS;
	};

/*
 * Free xpm data
 */
int
free_xpm_data (char ***data) {
	int i, ret;
	unsigned int columns, rows, colors;

	if (!data)
		return B_INPUT;

	ret = sscanf (*data[0], "%d %d %d", &columns, &rows, &colors);
	if (ret != 3)
		return B_INPUT;

	for (i = 0; i < 1 + colors + rows; i++) {
		if ((*data)[i] != NULL)
			free ((*data)[i]);

		(*data)[i] = NULL;
		};

	free (*data);
	*data = NULL;

	return B_SUCCESS;
	};
