#define COUNT_COLORS_OPTION 1

enum xpmb_errors {
	B_INPUT = -1,	/* input error					*/
	B_INSUF = -2,	/* too small buffer or not enough memory	*/
	B_INVAL = -3,	/* possibly invalid xpm file			*/
	B_NOSUP = -4,	/* not supported xpm file			*/
	B_FMISS = -5,	/* file not found				*/
	B_SUCCESS = 0
	};

typedef struct _xpm_image_t xpm_image_t;

/*
 * Simply read a file into memory
 */
int
read_file_to_buffer (char *filename, char **buffer);

/*
 * Remove C syntax from buffer, like if the file was included into source code
 */
int
parse_buffer_to_xpm_data (char *buffer, char ***data);

/*
 * Parse headers and colors. Data can be an xpm file included in C source
 */
int
parse_xpm_data_to_xpm_image (char **data, xpm_image_t **image);

/*
 * Prepare pixel data suitable for ximage
 */
int
parse_xpm_image_pixel_data (xpm_image_t *image, unsigned char **pixels);

/*
 * Change a color with symbolic name to value ...
 */
int
xpm_image_change_color_by_name (
		xpm_image_t *image, char *color_name, char *color_value);

/*
 * Change a color of old_value to new_value ...
 */
int
xpm_image_change_color_by_value (
		xpm_image_t *image, char *old_value, char *new_value);

#if COUNT_COLORS_OPTION
/*
 * Enable / disable color counting
 */
int
xpm_image_count_colors (xpm_image_t *image, int set_true_or_false);
#endif

int
xpm_image_get_colums (xpm_image_t *image);

int
xpm_image_get_rows (xpm_image_t *image);

int
xpm_image_get_number_of_colors (xpm_image_t *image);

int
xpm_image_get_chars_per_pixel (xpm_image_t *image);

/*
 * Set color value for 'None' in "#RRGGBBAA" format
 * Example : "#FF00FFFF"
 */
int
xpm_image_set_transparent_color (xpm_image_t *image, char *color);

/*
 * Copy symbolic name of the nth color to buffer upto length.
 * Returns number of copied bytes or negative error code.
 */
int
xpm_image_get_nth_color_name (
		xpm_image_t *image, int n, char *buffer, int length);

/*
 * Copy nth color value in "#RRGGBBAA" format to buffer upto length.
 * Returns number of copied bytes or negative error code.
 */
int
xpm_image_get_nth_color_value (
		xpm_image_t *image, int n, char *buffer, int length);

/*
 * Frees memory allocated during parsing xpm image data.
 * Also frees xpm_image_t, but leaves data untouched.
 */
int
free_xpm_image (xpm_image_t *image);

/*
 * Free xpm data
 */
int
free_xpm_data (char ***data);
