LIB_PATH = /usr/local/lib
INCL_PATH = /usr/local/include

# path to x11's rgb.txt file
RGB_PATH = /usr/share/X11

INSTALL = /usr/bin/install

# program name
# on version number change, remember to change it in reamde file too
lib = xpmb
major = 1
minor = 0

linkername = lib$(lib).so
soname = $(linkername).$(major)
realname = $(soname).$(minor)

short = xpmb
obj = $(short).o
header = $(short).h

CC = gcc

CFLAGS = -fpic -c -Wall

#debug
#CFLAGS += -g

help: commands

## commands: show accepted commands
commands:
	grep -E '^##' Makefile | sed -e 's/## //g'

## libxpmb.so.X.Y: compile libxpmb
$(realname): $(obj)
	$(CC) -shared -Wl,-soname,$(soname) -o $(realname) $(obj)  $(LDFLAGS)

$(obj): %.o: %.c
	$(CC) $(CFLAGS) -DSHARED_DIR='"$(RGB_PATH)"' -o $@ $<

## install: install libxpmb to dir set in Makefile
install: $(bin)
	$(INSTALL) -d $(LIB_PATH)/
	$(INSTALL) $(realname) $(LIB_PATH)
	ln -sf $(realname) $(LIB_PATH)/$(soname)
	ln -sf $(realname) $(LIB_PATH)/$(linkername)
	ldconfig
	$(INSTALL) -d $(INCL_PATH)/
	$(INSTALL) $(header) $(INCL_PATH)

## uninstall: remove installed files
uninstall:
	rm -r -f $(LIB_PATH)/$(realname) $(LIB_PATH)/$(soname) $(LIB_PATH)/$(linkername) $(INCL_PATH)/$(header)

## clean: remove compiled files (and not the installed ones)
clean:
	rm -f $(short).o $(realname)
